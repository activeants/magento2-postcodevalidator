# [![Active Ants](http://activeants.nl/content/themes/active-ants/img/logo.png)](http://activeants.nl/) 

## Magento 2 postcode validator
This module fixes the ugly "Provided Zip/Postal Code seems to be invalid" warning which is generated when the entered postcode doesn't contain a space, this module also filters out invalid postcodes using a leading 0 and postcodes using SA, SD and SS.

##Motivation
We have built a small collection of Dutch Magento 2 shops that use Dutch postcode validation and we always thought the "Provided Zip/Postal Code seems to be invalid" warning was really user-unfriendly and illogical, that's why we made this!

## Installation
```
composer require activeants/magento2-postcodevalidator
php bin/magento module:enable ActiveAnts_PostcodeValidator
php bin/magento setup:upgrade
```

## Issues
Feel free to report any issues for this project using the integrated [Bitbucket issue tracker](https://bitbucket.org/activeants/magento2-postcodevalidator/issues), we will try to get back to issues as fast as possible.

## License
The postcode validator is free software, and is released under the terms of the OSL 3.0 license. See LICENSE.md for more information.

## Credits
[Active Ants](http://activeants.nl/) provides e-commerce efulfilment services for webshops. We store products, pick, pack and ship them. But we do much more. With unique efulfilment solutions we make our clients, the webshops, more successful.

~ The [Active Ants](http://activeants.nl/) software development team.