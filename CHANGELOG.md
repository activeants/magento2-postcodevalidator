# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [1.0.0] - 2017-03-17
### Added
- Custom postcode validator using regex
- Changelog
- License
- Readme